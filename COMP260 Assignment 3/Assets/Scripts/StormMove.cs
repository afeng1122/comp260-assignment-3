﻿using UnityEngine;
using System.Collections;

public class StormMove : MonoBehaviour {

	public float stormSpeed = 0.02f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(stormSpeed * Time.deltaTime, 0, 0);
	}

	void OnCollisionEnter2D(Collision2D collision) { 
		Destroy(GameObject.FindWithTag("Player"));
	}

		
}
