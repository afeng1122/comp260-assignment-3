﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreKeeper : MonoBehaviour {

	public int scorePerRock = 1;
	private int score = 0;
	public Text scoreText;

	public AudioClip ping;
	public AudioSource source;

	void Awake(){
		source = GetComponent<AudioSource>();

	
	}


	void Start () {    
		// subscribe to events from all the Goals
		Delegate[] rocks = FindObjectsOfType<Delegate> ();

		for (int i = 0; i < rocks.Length; i++) {
			rocks[i].scoreEvent += OnScore;
		}

		score = 0;
		scoreText.text = "0";
	}

	public void OnScore(int player) {
		// add points to the player whose goal it is
		score += scorePerRock;
		scoreText.text = score.ToString ();
		Debug.Log ("Player " + player + ": " + score);
		source.PlayOneShot(ping);
	}

}
