﻿using UnityEngine;
using System.Collections;

public class Delegate : MonoBehaviour {
	
	public delegate void ScoreHandler(int player);
	public ScoreHandler scoreEvent;
	public int player;



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnTriggerEnter2D(Collider2D collider) {
	

		if (scoreEvent != null) {
			scoreEvent(player);
		}

		Destroy (gameObject);

	}


}
